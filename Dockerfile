FROM --platform=$BUILDPLATFORM tonistiigi/xx AS xx

FROM --platform=$BUILDPLATFORM golang:1.16-alpine AS builder

WORKDIR /tmp/go-build

COPY --from=xx / /
ARG TARGETPLATFORM
ENV CGO_ENABLED=0

COPY go.mod go.sum ./
RUN xx-go mod download

COPY . .
RUN xx-go build -o ./custom-error-pages . && xx-verify ./custom-error-pages

FROM alpine:3.13

COPY www /www/
COPY etc /etc/
COPY --from=builder /tmp/go-build/custom-error-pages /custom-error-pages
ENTRYPOINT ["/custom-error-pages"]
