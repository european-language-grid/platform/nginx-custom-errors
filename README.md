# Nginx Custom Errors

This is a simple extension of the standard [nginx ingress "custom error pages" example](https://github.com/kubernetes/ingress-nginx/tree/master/images/custom-error-pages) that can serve valid ELG JSON error messages when nginx generates a 502 bad gateway during a call to the restserver.
